## Instalation

```shell script
git config --global --add url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
go get bitbucket.org/VelasDevelopment/govelas
```

Compatible with go mod
   
## Use

Documentation in godoc

```shell script
godoc -http=:6060
```
 
and open http://localhost:6060
